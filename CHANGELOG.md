
# CHANGE LOG

For full in-detail view of the changes, please consult commits page:  
https://bitbucket.org/spinwheel/flutter-sdk/commits/branch/dev

# 2022

## 0.4.6 - March 11
- Added SWEnvironment for environment switching by partner

## 0.4.3 - February 23

- Added skipLandingPage and skipToServicerID parameters which allows the T&C Landing Page and the Servicer's List to be skipped, independently
- Several minor layout adjustments and bug-fixes
- Removed partner specific iconography

## 0.4.2 - February 16

- Downgraded Flutter Framework version down to 2.8 to conform to partners version
- Added retro-compatibility to our TextTheme approach to support both 2.8 and 2.10 Flutter Framework versions

## 0.4.1 - February 10

- Validated full support for Mobile and Desktop devices, asserting a safe area is respected when presenting content
- Validated support for AidVantage loan servicer
- Updated static assets
- Added Theming Capabilities to Mobile SDK & Loan Connect
- Added Text Theme capabilities, allowing for common styles (heading, body, label et cetera) to be applied globally as a theme
- Added customizable screen transition support, allowing for the partner to fixate a specific set of transitions (material, cupertino et cetera) or allow the native one.
- Connect Module: added full customization options on the Landing Page, allowing the partner to replace parts or the whole layout, as well as stylize with maximum flexibility
- Docs: added [lts_customization.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/lts_customization.md) regarding long-term support for partner customizations

## 0.4.0 - February 2

- Added setup option to toggle on/off the logging capability
- Added support for text input styling control
- Layout adjustments to match partners specs
- Exposed onBackPressed callback via IconButton
- Styling change for better support from Flutter Framework
- Added sample_style.json in assets for theming approach sample
- Docs: added [publishing.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/publishing.md) regarding publishing to pub.dev vs self hosted
- Docs: added [lts_layout.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/publishing.md) regarding long-term support for custom layouts
- Docs: added [lts_config.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/publishing.md) regarding long-term support for partner configurations

## 0.3.0 - January 26

- Added Consumer App Testing Environment, which behaves like a "debug mode app" to which several auxiliary behaviours have been added for easier centralized testing
- Added testable logged user scenario and showcased how to persist User object once logged in
- Added support for customizable partner logo in security screen
- Added unintegrated login flow for non-listed servicers
- Added date picker to date of birth fields
- Changed error feedback to an error box
- Changed asset approach to a partner-provided Widget at setup
- Changed Dependency Injection solution. Now using Injector.
- Docs: added [lts_styling.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/publishing.md) regarding long-term support for theming
- Several minor layout and style adjustments
- Several minor bugs

# 2021

## 0.2.0 - December 27

- created two additional repos for specific partners
- removed any and all partner data from flutter-sdk

## 0.1.5 - December 24

- added partner specific config files to make loan_connect consumer agnostic
- login errors are now exposed in full to partners
- created dev branch on all packages
- created test branch on all packages
- dev branches now depend on dev branches
- test branches now depend on test branches
- release branches continue to depend only on release branches

## 0.1.4 - December 16

- added new settings for better theming
- changed remote host to use https instead of http

## 0.1.3 - December 10

- added sample remote hosts to test branch
- created run_loan_connect_nav_test

## 0.1.2 - December 8

- simplified usage samples for easier understanding
- moved to using RESOLVED json assets instead of fetching them from assets
- partner should provide its own resolved theming asset as a resolved JSON
- started using https dependencies instead of ssh ones

## 0.1.1 - December 2

- added remote host support for proper testing with any device
- removed local express server dependency for testing
- removed dependencies
- test branch depends on develop branch of other packages

## 0.1.0 - December 1

- Initial release comprised of generic run configs as well as partner specific ones.
- Using GetX for DI, Navigation and State Management.
- Pending on corner case login scenarios.