import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/service/i_service_theme.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/routes/route_landing_page.dart';
import 'package:sw_loan_connect/routes/route_login.dart';
import 'package:sw_loan_connect/routes/route_servicers.dart';

final RouteObserver<ModalRoute<void>> routeObserverLandingPage = RouteObserver<ModalRoute<void>>();
final RouteObserver<ModalRoute<void>> routeObserverServicers = RouteObserver<ModalRoute<void>>();
final RouteObserver<ModalRoute<void>> routeObserverLogin = RouteObserver<ModalRoute<void>>();

class LoanConnectApp extends MaterialApp {
  LoanConnectApp({Key? key}) : super(key: key);

  @override
  GlobalKey<NavigatorState>? get navigatorKey => navKey;

  @override
  Widget get home {
    final params = get<ParamsLoanConnect>();
    if (params.skipLandingPage) {
      return params.skipToServicerID == null ? RouteServicers() : RouteLogin();
    }
    return RouteLandingPage();
  }

  @override
  List<NavigatorObserver>? get navigatorObservers =>
      [routeObserverLandingPage, routeObserverServicers, routeObserverLogin];

  @override
  String get title => 'Loan Connect';

  @override
  bool get debugShowCheckedModeBanner => false;

  @override
  ThemeMode get themeMode => ThemeMode.system;

  @override
  ThemeData get theme => get<IServiceTheme>().theme;

  @override
  ThemeData get darkTheme => get<IServiceTheme>().darkTheme;
}
