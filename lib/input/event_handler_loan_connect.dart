import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/tool/inject.dart';

class EventHandlerLoanConnect {
  final authStep = get<IUseCaseAuth>().authStep;
  final error = get<IUseCaseAuth>().error;

  void Function()? onPolicy;
  void Function()? onExit;
  void Function()? onConnectAnotherLoan;
  void Function()? onContinue;
  void Function()? onContinueUnintegrated;
}
