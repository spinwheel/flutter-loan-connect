import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/better_button.dart';
import 'package:sw_component/components/widget/exit_header.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/logo_container.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/input/event_handler_loan_connect.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_landing_page.dart';

class LayoutLandingPageB extends StatelessWidget {
  LayoutLandingPageB(this._viewModel, {Key? key}) : super(key: key);

  final ViewModelLandingPage _viewModel;

  final TextStyle? privacyPolicyLink = theme.textTheme.bodyText2;

  BuildContext? context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return MaterialThemeWrapper(
      child: Scaffold(
        backgroundColor: colorScheme.background,
        body: _buildBody(context),
        bottomNavigationBar: _viewModel.params.navButton ??
            BetterButton(text: "Continue", onPressed: () => _viewModel.onContinue(context)),
      ),
    );
  }

  _buildBody(BuildContext context) => SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: _outerColumn(context),
        ),
      );

  _outerColumn(BuildContext context) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: _innerColumn(context),
          ),
        ],
      );

  _innerColumn(BuildContext context) => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ..._header(),
            ..._cards(),
            ..._footer(),
          ],
        ),
      );

  _header() => [
        SizedBox(
          height: 5,
        ),
        _viewModel.params.hasExitButton
            ? ExitHeader(onExit: getSafe<EventHandlerLoanConnect>()?.onExit)
            : Container(),
        SizedBox(
          height: 30,
        ),
        Row(
          children: [
            const LogoContainer(
              uri: 'logo_spinwheel.png',
              package: 'sw_loan_connect',
            ),
            SizedBox(
              width: 15,
            ),
            _viewModel.params.linkIcon ??
                ImageCompat(
                  'chain_icon.png',
                  package: 'sw_loan_connect',
                  width: 20,
                  height: 10,
                ),
            SizedBox(
              width: 15,
            ),
            LogoContainer(
              child: _viewModel.params.logo,
              width: 42.67,
              height: 36.10,
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        _viewModel.params.title ??
            Text(
              'Connect your student loan servicer\n',
              style: theme.textTheme.headline4,
              textAlign: TextAlign.start,
            ),
        OrientationBuilder(
          builder: (context, orientation) =>
              orientation == Orientation.landscape ? const SizedBox(height: 36) : SizedBox(),
        )
      ];

  _cards() => [
        _viewModel.params.iconText0 ??
            Row(
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: [
                ImageCompat(
                  'locker_logo.png',
                  package: 'sw_loan_connect',
                  width: 16,
                  height: 21,
                  color: Colors.black,
                ),
                SizedBox(
                  width: 12,
                ),
                Flexible(
                  child: Text(
                    'Get a secure connection with read-only access',
                    style: theme.textTheme.bodyText1,
                  ),
                )
              ],
            ),
        const SizedBox(height: 16),
        _viewModel.params.iconText1 ??
            Row(
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: [
                ImageCompat(
                  'shield_logo.png',
                  package: 'sw_loan_connect',
                  width: 16,
                  height: 21,
                ),
                SizedBox(
                  width: 12,
                ),
                Flexible(
                  child: Text(
                    '${_viewModel.params.partnerName} will never store your credentials',
                    style: theme.textTheme.bodyText1,
                  ),
                )
              ],
            ),
      ];

  _footer() => [
        const SizedBox(height: 30),
        _viewModel.params.disclaimerText ??
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text:
                      'By clicking “Continue” you agree to the Consent and Privacy Policy, and you consent and authorize Spinwheel to take actions with your loan service provider account as set forth in the ',
                  style: theme.textTheme.bodyText2,
                ),
                TextSpan(
                    text: 'Consent and Privacy Policy',
                    style: TextStyle(
                      color: colorScheme.primary,
                      height: theme.textTheme.bodyText2!.height,
                      fontFamily: theme.textTheme.bodyText2!.fontFamily,
                      fontWeight: theme.textTheme.bodyText2!.fontWeight,
                      fontSize: theme.textTheme.bodyText2!.fontSize,
                      fontStyle: theme.textTheme.bodyText2!.fontStyle,
                      locale: theme.textTheme.bodyText1!.locale,
                      textBaseline: theme.textTheme.bodyText1!.textBaseline,
                    ),
                    recognizer: TapGestureRecognizer()..onTap = _viewModel.privacyPolicyLink)
              ]),
            ),
      ];
}
