import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/better_button.dart';
import 'package:sw_component/components/widget/date_picker.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/item_form_field.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/result_screen.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_core/entity_auth/auth_step_params.dart';
import 'package:sw_core/entity_auth/login_security_details.dart';
import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_auth/security_details.dart';
import 'package:sw_core/entity_servicer/auth_params.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/input/event_handler_loan_connect.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_login.dart';

class LayoutLoginB extends StatelessWidget {
  LayoutLoginB(this._viewModel, {Key? key}) : super(key: key);

  final ViewModelLogin _viewModel;

  final ParamsLoanConnect params = get<ParamsLoanConnect>();
  final BlueDatePicker _datePicker = BlueDatePicker(showLabel: true, label: "Date of Birth");
  final _userBox = BlueItemFormField(
    label: "Username",
  ).obs;
  final BlueItemFormField _passwordBox = BlueItemFormField(
    label: "Password",
    obscureText: true,
  );
  final scrollController = ScrollController();

  final Map<String, String> _assets = {
    "9422554f-0170-4afe-b8df-12aaecc3fdef": "logo_aes.png",
    "e32be248-d43f-4dd4-9e34-05c7b8ab1888": "logo_firstmark.png",
    "bb3e39e7-9dd6-461e-a4f4-c95a1a1e80ef": "logo_nelnet.png",
    "e35a6454-ca66-4b35-a7e7-afb25d03c424": "logo_suntrust.png",
  };

  @override
  Widget build(BuildContext context) {
    return MaterialThemeWrapper(
      child: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) => Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            child: Observer(() => _checkAuthStep(context)),
          )
        ],
      );

  Widget _checkAuthStep(BuildContext context) {
    _viewModel.applyRules();
    switch (_viewModel.usecaseAuth.authStep.value) {
      case AuthStep.STEP_UNAUTHORIZED:
        return _initialState(false);
      case AuthStep.STEP_SECURITY:
        return _authStepSecurity(context, false);
      case AuthStep.STEP_AUTHORIZED:
        _viewModel.onAuthSuccessful();
        return _authCompleted(context);
      case AuthStep.AUTH_ERROR:
        return _authError(context);
      default:
        return _initialState(false);
    }
  }

  Widget _initialState(bool hasError) {
    _viewModel.usecaseAuth.authStep.value = AuthStep.STEP_UNAUTHORIZED;
    return Scaffold(
      backgroundColor: colorScheme.background,
      bottomNavigationBar: _continueButtonInitialState(),
      body: Flex(
        direction: Axis.vertical,
        mainAxisSize: MainAxisSize.max,
        children: [
          SlimAppBar(''),
          Expanded(
              child: SingleChildScrollView(
                  controller: scrollController, child: _loginFields(hasError))),
        ],
      ),
    );
  }

  Widget _continueButtonInitialState() => Observer(
        () => IgnorePointer(
          ignoring: _viewModel.sendButtonClicked.value,
          child: BetterButton(
              text: "Connect loan",
              onPressed: () async => await _viewModel.sendRequestAuth(
                  _userBox.value.field.controller!.text,
                  _passwordBox.field.controller!.text,
                  _datePicker.selectedDate),
              isLoading: _viewModel.usecaseAuth.isPolledTask.value &&
                      _viewModel.usecasePolledAuth!.isPolling.value ||
                  _viewModel.sendButtonClicked.value),
        ),
      );

  Widget _authStepSecurity(BuildContext context, bool hasError) {
    _viewModel.usecaseAuth.authStep.value = AuthStep.STEP_SECURITY;
    final SecurityStep security = _viewModel.usecaseAuth.security.value;

    List<BlueItemFormField> formItems = security.securityDetails
        .asMap()
        .entries
        .map(
          (entry) => BlueItemFormField(
            label: entry.value.question,
            initialValue: entry.value.answer,
          ),
        )
        .toList();

    return Center(
      child: Scaffold(
        backgroundColor: colorScheme.background,
        appBar: SlimAppBar('', onBackPressed: () => _viewModel.onBackPressed(context)),
        bottomNavigationBar: _continueButtonSecurityQuestions(security, formItems),
        body: Center(
          child: SingleChildScrollView(
            controller: scrollController,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 16),
                  child: _servicerLogo(),
                ),
                hasError
                    ? _viewModel.createErrorBox(
                        margin: EdgeInsets.symmetric(horizontal: 32),
                        message: 'One or more answers are incorrect. Kindly check & try again.')
                    : SizedBox(),
                ...formItems,
                const SizedBox(height: 16),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _continueButtonSecurityQuestions(
          SecurityStep security, List<BlueItemFormField> formItems) =>
      Observer(
        () => IgnorePointer(
          ignoring: _viewModel.sendButtonClicked.value,
          child: BetterButton(
              text: "Continue",
              isLoading: _viewModel.sendButtonClicked.value,
              onPressed: () {
                _viewModel.requestAuthAddSteps(
                    security,
                    AuthStepParams(accountUserName: security.nextAuthStepParams.accountUserName),
                    LoginSecurityDetails(
                      securityDetails: List.generate(
                        security.securityDetails.length,
                        (index) => SecurityDetails(
                          questionId: security.securityDetails[index].questionId,
                          question: security.securityDetails[index].question,
                          answer: formItems[index].field.controller?.text,
                        ),
                      ),
                    ));
                _viewModel.sendButtonClicked.value = true;
              }),
        ),
      );

  Widget _authCompleted(BuildContext context) => ResultScreen(
      hasExitHeader: true,
      onExit: getSafe<EventHandlerLoanConnect>()?.onExit ?? () => _viewModel.onBackPressed(context),
      icon: ImageCompat(
        'success.svg',
        package: 'sw_loan_connect',
        width: 60,
        height: 60,
      ),
      title: 'Student loan connected',
      firstButtonText: 'Connect another loan',
      firstButtonPressed: () => _viewModel.onAddAnotherLoan(context)(),
      secondButtonText: 'View my account',
      secondButtonPressed: () => _viewModel.onContinueAuthSuccess(context)());

  Widget _loginFields(bool hasError) => Flex(
        direction: Axis.vertical,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _servicerLogo(),
          Padding(
            padding: EdgeInsets.only(left: 16),
            child: Text(
              'Enter your loan information',
              style: theme.textTheme.headline4,
              textAlign: TextAlign.left,
            ),
          ),
          _viewModel.checkBoxes(hasError),
          SizedBox(height: 32),
          _changeUserBoxLabel(),
          _passwordBox,
          ..._authParamsFields(),
          _forgotPasswordLink(),
          const SizedBox(height: 64),
        ],
      );

  _changeUserBoxLabel() => _viewModel.isUserIDInsteadOfUserName.value
      ? _userBox.value = BlueItemFormField(label: 'User ID')
      : _userBox.value;

  _authParamsFields() {
    _viewModel.authParams.clear();

    _viewModel.createTxtControllers(_viewModel.servicer);

    final fields = _viewModel
        .mapAuthStepsFields((
          param,
        ) =>
            _viewModel.servicer.authSteps[0].authParams.length > 4 && param.key == 3
                ? _checkAddORLabel(param)
                : _checkAddDatePicker(param))
        .toList();

    return fields;
  }

  _checkAddORLabel(MapEntry<int, AuthParams> param) => Column(
        children: [
          RawBlueItemFormField(
              label: param.value.label,
              showHint: true,
              controller: param.key <= _viewModel.servicer.authSteps[0].authParams.length
                  ? _viewModel.authParams[param.key]
                  : null),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: Text(
              "OR",
              style: theme.textTheme.headline4,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      );

  _checkAddDatePicker(MapEntry<int, AuthParams> param) =>
      _viewModel.servicer.authSteps[0].authParams.length > 4 && param.key == 2
          ? _datePicker
          : _viewModel.servicer.authSteps[0].authParams.length >= 4 && param.key == 2
              ? _datePicker
              : RawBlueItemFormField(
                  label: param.value.label,
                  showHint: false,
                  controller: param.key <= _viewModel.servicer.authSteps[0].authParams.length
                      ? _viewModel.authParams[param.key]
                      : null);

  Widget _servicerLogo() => Container(
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.only(top: 21, bottom: 27, left: 16),
        child: ImageCompat(
          _assets.containsKey(_viewModel.servicer.id)
              ? _assets[_viewModel.servicer.id]!
              : _viewModel.servicer.logoUrl.toString(),
          height: 60,
          width: 80,
          fit: BoxFit.fitWidth,
          package: _assets.containsKey(_viewModel.servicer.id) ? "sw_loan_connect" : '',
        ),
      );

  Widget _forgotPasswordLink() => Observer(
        () => _viewModel.canLaunchForgotPassword.value
            ? Padding(
                padding: const EdgeInsets.only(
                  left: 16,
                  top: 4,
                  right: 16,
                  bottom: 16,
                ),
                child: GestureDetector(
                  onTap: _viewModel.forgotPasswordLink,
                  child: Text(
                    'Forgot password?',
                    style: TextStyle(
                      color: colorScheme.primary,
                      fontStyle: theme.textTheme.bodyText1!.fontStyle,
                      fontSize: theme.textTheme.bodyText1!.fontSize,
                      fontWeight: theme.textTheme.bodyText1!.fontWeight,
                      fontFamily: theme.textTheme.bodyText1!.fontFamily,
                      height: theme.textTheme.bodyText1!.height,
                      locale: theme.textTheme.bodyText1!.locale,
                      textBaseline: theme.textTheme.bodyText1!.textBaseline,
                    ),
                  ),
                ),
              )
            : Center(),
      );

  _authError(BuildContext context) {
    _viewModel.setVisibilityError(true);
    _viewModel.loginAttempts.value++;
    if (_viewModel.loginAttempts.value >= 3) {
      _viewModel.setVisibilityWarning(true);
    }
    _viewModel.scrollToTop(scrollController);
    return _viewModel.usecaseAuth.security.value.securityDetailsRequired != null &&
            _viewModel.usecaseAuth.security.value.securityDetailsRequired!
        ? _authStepSecurity(context, true)
        : _initialState(true);
  }
}
