import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/box_card.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/logo_container.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_landing_page.dart';

class LayoutLandingPageDefault extends StatelessWidget {
  LayoutLandingPageDefault(this._viewModel, {Key? key}) : super(key: key);

  final ViewModelLandingPage _viewModel;

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
        child: Scaffold(
          backgroundColor: colorScheme.background,
          body: _buildBody(),
          bottomNavigationBar: _viewModel.params.navButton ??
              SizedBox(
                height: 64,
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: colorScheme.secondary,
                    onSurface: colorScheme.secondary,
                  ),
                  onPressed: () => _viewModel.onContinue(context),
                  child: Text(
                    'Continue',
                    style: TextStyle(
                      color: colorScheme.onPrimary,
                    ),
                  ),
                ),
              ),
        ),
      );

  _buildBody() => SingleChildScrollView(
        child: Center(
          child: _outerColumn(),
        ),
      );

  _outerColumn() => Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: _innerColumn(),
          ),
        ],
      );

  _innerColumn() => Column(
        children: [
          ..._header(),
          ..._cards(),
          ..._footer(),
        ],
      );

  _header() => [
        _viewModel.params.title ?? const Text('Connect Loan', style: TextStyle(fontSize: 26)),
        const Padding(
          padding: EdgeInsets.only(left: 56, top: 16, right: 56, bottom: 16),
          child: Text(
            'Connect with your student loan servicer to securely sync your loan data.',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16),
          ),
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            LogoContainer(
              child: _viewModel.params.logo,
            ),
            Stack(
              alignment: Alignment.center,
              children: [
                const ImageCompat(
                  'shield_solid.svg',
                  package: 'sw_loan_connect',
                  width: 64,
                  height: 64,
                  color: Colors.white,
                ),
                ImageCompat(
                  'logo_spinwheel.png',
                  package: 'sw_loan_connect',
                  width: 42,
                  height: 42,
                  fit: BoxFit.fill,
                ),
              ],
            ),
            Stack(
              alignment: Alignment.center,
              children: [
                ImageCompat(
                  'circle.svg',
                  package: 'sw_loan_connect',
                  width: 64,
                  height: 64,
                  color: Colors.white,
                ),
                ImageCompat(
                  'bank.svg',
                  package: 'sw_loan_connect',
                  width: 42,
                  height: 36,
                ),
              ],
            ),
          ],
        ),
      ];

  _cards() => [
        const SizedBox(height: 30),
        Container(
          width: 354,
          child: BoxCard(
            margin: EdgeInsets.zero,
            child: Container(
              width: 354,
              child: Text(
                '${_viewModel.params.partnerName} securely connects to your loan servicer through Spinwheel',
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 14),
              ),
            ),
          ),
        ),
        const SizedBox(height: 30),
        Container(
          width: 354,
          height: 150,
          child: BoxCard(
            margin: EdgeInsets.zero,
            child: _viewModel.params.iconText0 ??
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(left: 10, top: 6),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              ImageCompat(
                                'circle.svg',
                                package: 'sw_loan_connect',
                                width: 26,
                                height: 26,
                                color: theme.primaryColor,
                              ),
                              ImageCompat(
                                'checkmark.svg',
                                package: 'sw_loan_connect',
                                width: 12,
                                height: 12,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 10, top: 6),
                          child: const Text(
                            'Secure',
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20, top: 20),
                      child: Text(
                        'Transfer of your information is encrypted end-to-end',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  ],
                ),
          ),
        ),
        const SizedBox(height: 30),
        Container(
          width: 354,
          height: 150,
          child: BoxCard(
            margin: EdgeInsets.zero,
            child: _viewModel.params.iconText1 ??
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(left: 10, top: 6),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              ImageCompat(
                                'circle.svg',
                                package: 'sw_loan_connect',
                                width: 26,
                                height: 26,
                                color: theme.primaryColor,
                              ),
                              ImageCompat(
                                'checkmark.svg',
                                package: 'sw_loan_connect',
                                width: 12,
                                height: 12,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 10, top: 6),
                          child: const Text(
                            'Private',
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20, top: 20),
                      child: Text(
                        'Your credentials will never be made accessible to ${_viewModel.params.partnerName.toLowerCase()}',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  ],
                ),
          ),
        ),
        const SizedBox(height: 30),
        Container(
          width: 354,
          height: 150,
          child: BoxCard(
            margin: EdgeInsets.zero,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 4.0),
                      child: ImageCompat(
                        'Figure.svg',
                        package: 'sw_loan_connect',
                        width: 16,
                        height: 16,
                        fit: BoxFit.fitWidth,
                        color: theme.primaryColor,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 10),
                      child: const Text(
                        'Maximize principal pay down',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 13),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 18),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Flexible(
                        child: Container(
                      margin: EdgeInsets.only(right: 10, left: 4),
                      child: ImageCompat(
                        'arrows.svg',
                        package: 'sw_loan_connect',
                        height: 16,
                        fit: BoxFit.fitWidth,
                        color: theme.primaryColor,
                      ),
                    )),
                    const Text(
                      'Ensure it\'s an extra loan payment',
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 13),
                    )
                  ],
                ),
                const SizedBox(height: 16),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Stack(
                        alignment: Alignment.topCenter,
                        children: [
                          ImageCompat(
                            'bookmark.svg',
                            package: 'sw_loan_connect',
                            width: 9,
                            height: 9,
                            fit: BoxFit.fitWidth,
                            color: theme.primaryColor,
                          ),
                          ImageCompat(
                            'arrow_percent.svg',
                            package: 'sw_loan_connect',
                            width: 16,
                            height: 16,
                            fit: BoxFit.fitWidth,
                            color: theme.primaryColorDark,
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                        child: Text(
                      'Pay down the most expensive loans first',
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 13),
                    )),
                  ],
                ),
              ],
            ),
          ),
        ),
      ];

  _footer() => [
        const SizedBox(height: 30),
        _viewModel.params.disclaimerText ??
            Column(
              children: [
                const Text(
                  'By clicking “Continue” you agree to the Consent and Privacy Policy, and you consent and authorize Spinwheel to take actions with your loan service provider account as set forth in the ',
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 4),
                TextButton(
                  onPressed: _viewModel.privacyPolicyLink,
                  child: Text(
                    'Consent and Privacy Policy',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: colorScheme.primary),
                  ),
                ),
              ],
            ),
      ];
}
