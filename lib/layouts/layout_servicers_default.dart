import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/item_security_question.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_servicers.dart';

class LayoutServicersDefault extends StatelessWidget {
  const LayoutServicersDefault(this._viewModel, {Key? key}) : super(key: key);

  final ViewModelServicers _viewModel;

  @override
  Widget build(BuildContext context) {
    _viewModel.getAllServicers();
    return MaterialThemeWrapper(
        child: Scaffold(
      backgroundColor: colorScheme.background,
      appBar: SlimAppBar('Select your loan servicer'),
      body: _body(context),
    ));
  }

  Widget _body(BuildContext context) => Column(
        children: [
          Row(
            children: [
              Expanded(
                child: _searchbar(),
              ),
            ],
          ),
          Flexible(
            child: Observer(() => Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                  child: GridView.count(
                    crossAxisCount: 2,
                    shrinkWrap: true,
                    mainAxisSpacing: 16,
                    crossAxisSpacing: 16,
                    childAspectRatio: 5 / 4,
                    children: _viewModel.loadServicerList(context),
                  ),
                )),
          )
        ],
      );

  Widget _searchbar() => Container(
        margin: EdgeInsets.only(bottom: 32),
        child: ItemFormField(
            showHint: true,
            hint: 'Search',
            onChanged: (string) => _viewModel.search.value = string.toLowerCase()),
      );
}
