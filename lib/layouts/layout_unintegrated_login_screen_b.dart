import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/date_picker.dart';
import 'package:sw_component/components/widget/item_form_field.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/result_screen.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_component/components/widget/status_box.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_auth/auth_step_params.dart';
import 'package:sw_core/entity_auth/login_security_details.dart';
import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_auth/security_details.dart';
import 'package:sw_core/entity_user/unintegrated_user.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_unintegrated_login.dart';

import '../input/event_handler_loan_connect.dart';

class UnintegratedLoginScreen extends StatelessWidget {
  UnintegratedLoginScreen(this._viewModel, {Key? key}) : super(key: key) {
    put<ViewModelUnintegratedLogin>(() => ViewModelUnintegratedLogin());
  }

  final ViewModelUnintegratedLogin _viewModel;

  final RawBlueItemFormField _firstName = _newField("First name");
  final RawBlueItemFormField _lastName = _newField("Last name");
  final RawBlueItemFormField _emailID = _newField("Email ID");
  final RawBlueItemFormField _loanServicerName = _newField("Loan servicer name *");
  final RawBlueItemFormField _username = _newField("Username *");
  final RawBlueItemFormField _password = _newField("Password *", obscureText: true);
  final RawBlueItemFormField _pin = _newField("Pin");
  final BlueDatePicker _datePicker = BlueDatePicker(
    showLabel: true,
    label: "Date of birth",
  );
  final RawBlueItemFormField _accountNumber = _newField("Account number");

  final ScrollController _scrollController = ScrollController();
  BuildContext? context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return MaterialThemeWrapper(
        child: Scaffold(
      backgroundColor: colorScheme.background,
      body: Observer(() => _checkAuthStep(context)),
    ));
  }

  _checkAuthStep(BuildContext context) {
    put<ViewModelUnintegratedLogin>(() => ViewModelUnintegratedLogin());

    switch (_viewModel.usecaseAuth!.authStep.value) {
      case AuthStep.STEP_UNAUTHORIZED:
        return _initialState(context, false);
      case AuthStep.STEP_SECURITY:
        return _hasSecurityQuestions(context);
      case AuthStep.STEP_AUTHORIZED:
        return _authCompleted(context);
      case AuthStep.AUTH_ERROR:
        return _authError(context);
    }
  }

  Widget _hasSecurityQuestions(BuildContext context) {
    if (_viewModel.usecaseAuth!.security.value.securityDetails.isNotEmpty) {
      _viewModel.usecaseAuth!.authStep.value = AuthStep.STEP_SECURITY;
      return _authStepSecurity(context);
    } else {
      _viewModel.usecaseAuth!.unintegratedUser.value = UnintegratedUser(
        firstName: _firstName.controller!.text,
        lastName: _lastName.controller!.text,
        emailID: _emailID.controller!.text,
        loanServicerName: _loanServicerName.controller!.text,
        username: _username.controller!.text,
        userId: _username.controller!.text,
        password: _password.controller!.text,
        pin: _pin.controller!.text,
        dob: _datePicker.selectedDate == formatNumbersUSA(DateTime.now().toString())
            ? null
            : _datePicker.selectedDate,
        accountNumber: _accountNumber.controller!.text,
      );
      _viewModel.usecaseAuth!.authStep.value = AuthStep.STEP_AUTHORIZED;
      return _authCompleted(context);
    }
  }

  Widget title() => Container(
        margin: EdgeInsets.only(top: 16, bottom: 20, left: 16),
        child: Text(
          'Enter your loan information',
          style: theme.textTheme.headline4,
        ),
      );

  List<Widget> _formBody() => [
        _firstName,
        _lastName,
        _emailID,
        _loanServicerName,
        _username,
        _password,
        _pin,
        _datePicker,
        _accountNumber
      ];

  Widget _initialState(BuildContext context, bool hasError) => SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SlimAppBar(
              '',
              onBackPressed: () => _viewModel.onBackPressed(context),
            ),
            title(),
            hasError ? _createErrorBox() : SizedBox(),
            ..._formBody(),
            SizedBox(height: 56),
            _submitButton()
          ],
        ),
      );

  _authStepSecurity(BuildContext context) {
    _viewModel.usecaseAuth!.authStep.value = AuthStep.STEP_SECURITY;
    final SecurityStep security = _viewModel.usecaseAuth!.security.value;

    List<BlueItemFormField> formItems = security.securityDetails
        .asMap()
        .entries
        .map(
          (entry) => BlueItemFormField(
            label: entry.value.question,
            initialValue: entry.value.answer,
          ),
        )
        .toList();

    return Center(
      child: Scaffold(
        backgroundColor: colorScheme.background,
        appBar: SlimAppBar(
          '',
          onBackPressed: () => _viewModel.onBackPressed(context),
        ),
        bottomNavigationBar: _continueButtonSecurityQuestions(security, formItems),
        body: Center(
          child: SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ...formItems,
                const SizedBox(height: 16),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _continueButtonSecurityQuestions(
          SecurityStep security, List<BlueItemFormField> formItems) =>
      IgnorePointer(
        ignoring: _viewModel.isLoading.value,
        child: Container(
          margin: EdgeInsets.only(left: 30, right: 30, bottom: 16),
          height: 48,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              splashFactory: NoSplash.splashFactory,
              onPrimary: colorScheme.secondary,
              onSurface: colorScheme.secondary,
            ),
            onPressed: () async {
              await _viewModel.requestAuthAddSteps(
                security,
                AuthStepParams(accountUserName: security.nextAuthStepParams.accountUserName),
                LoginSecurityDetails(
                  securityDetails: List.generate(
                    security.securityDetails.length,
                    (index) => SecurityDetails(
                      questionId: security.securityDetails[index].questionId,
                      question: security.securityDetails[index].question,
                      answer: formItems[index].field.controller?.text,
                    ),
                  ),
                ),
                _loanServicerName.controller!.text,
              );
              _viewModel.isLoading.value = true;
            },
            child: Observer(() => _viewModel.isLoading.value
                ? _loading()
                : Text(
                    'Continue',
                    style: TextStyle(
                      color: colorScheme.onPrimary,
                    ),
                  )),
          ),
        ),
      );

  Widget _authError(BuildContext context) {
    _viewModel.setVisibility(true);
    _viewModel.scrollToTop(_scrollController);
    return _initialState(context, true);
  }

  _authCompleted(BuildContext context) => Scaffold(
      backgroundColor: colorScheme.background,
      appBar: SlimAppBar(
        '',
        onBackPressed: () => _viewModel.onBackPressed(context),
      ),
      body: ResultScreen(
          hasExitHeader: false,
          icon: _viewModel.params.servicerNotFoundIcon,
          title: 'We can’t find your servicer',
          bodyText:
              'Looks like we couldn’t find your loan servicer. You can try again or continue without connecting.',
          firstButtonPressed: () => _viewModel.onBackPressed(context),
          secondButtonPressed: getSafe<EventHandlerLoanConnect>()?.onContinueUnintegrated ??
              () {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    behavior: SnackBarBehavior.floating,
                    content: Text("Replace me with a navigation callback!"),
                  ),
                );
              },
          firstButtonText: 'Connect a different loan',
          secondButtonText: 'Continue without connecting'));

  Widget _submitButton() => Container(
        height: 48,
        width: double.infinity,
        margin: EdgeInsets.only(left: 16, right: 16, bottom: 24),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(splashFactory: NoSplash.splashFactory),
          child: Observer(() => _viewModel.isLoading.value
              ? _loading()
              : Text('Submit', style: TextStyle(fontSize: 16))),
          onPressed: () => _viewModel.sendRequestAuth(
            _emailID.controller!.text,
            _firstName.controller!.text,
            _lastName.controller!.text,
            _loanServicerName.controller!.text,
            _username.controller!.text,
            _password.controller!.text,
            _pin.controller!.text,
            _datePicker.selectedDate,
            _accountNumber.controller!.text,
          ),
        ),
      );

  Widget _loading() => CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );

  Widget _createErrorBox({String? message, EdgeInsets? margin}) => Center(
        child: Container(
          margin: margin ?? EdgeInsets.only(left: 16, right: 16, top: 20),
          child: Observer(
            () => _viewModel.errorBoxVisibility.value
                ? StatusBox(
                    _viewModel.reset,
                    child: Text(
                      message ??
                          "You must fill at least the loan servicer name, username and password. Kindly recheck.",
                      style: TextStyle(
                        color: const Color(0xff721D24),
                      ),
                    ),
                    width: double.infinity,
                  )
                : Container(),
          ),
        ),
      );
}

_newField(String label, {bool? obscureText}) => RawBlueItemFormField(
      obscureText: obscureText ?? false,
      controller: TextEditingController(),
      label: label,
      showHint: false,
    );
