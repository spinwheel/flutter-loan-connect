import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/date_picker.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/item_security_question.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_component/components/widget/sw_dialog.dart';
import 'package:sw_core/entity_auth/auth_step_params.dart';
import 'package:sw_core/entity_auth/login_security_details.dart';
import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_auth/security_details.dart';
import 'package:sw_core/entity_servicer/auth_params.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_login.dart';

class LayoutLoginDefault extends StatelessWidget {
  LayoutLoginDefault(this._viewModel, {Key? key}) : super(key: key);

  final ViewModelLogin _viewModel;

  final DatePicker _datePicker = DatePicker();

  final _userBox = ItemFormField(
    showHint: true,
    hint: "Username",
  ).obs;

  final ItemFormField _passwordBox = ItemFormField(
    hint: "Password",
    showHint: true,
    obscureText: true,
  );
  final scrollController = ScrollController();

  final List<Widget> _inGreyContainer = [];
  final Map<String, String> _assets = {
    "9422554f-0170-4afe-b8df-12aaecc3fdef": "logo_aes.png",
    "e32be248-d43f-4dd4-9e34-05c7b8ab1888": "logo_firstmark.png",
    "bb3e39e7-9dd6-461e-a4f4-c95a1a1e80ef": "logo_nelnet.png",
    "e35a6454-ca66-4b35-a7e7-afb25d03c424": "logo_suntrust.png",
  };
  final String sallieMaeId = "09945206-21fc-4e8b-9779-be328c0926d5";

  @override
  Widget build(BuildContext context) {
    return MaterialThemeWrapper(
      child: Scaffold(
        backgroundColor: colorScheme.background,
        appBar: _buildAppBar(context),
        body: _buildBody(context),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) =>
      SlimAppBar('We are ready to GO!', onBackPressed: () => _viewModel.onBackPressed(context));

  Widget _buildBody(BuildContext context) => Observer(() => _checkAuthStep(context));

  Widget _checkAuthStep(BuildContext context) {
    _viewModel.applyRules();
    switch (_viewModel.usecaseAuth.authStep.value) {
      case AuthStep.STEP_SECURITY:
        return _authStepSecurity(false);
      case AuthStep.AUTH_ERROR:
        return _authError(context);
      case AuthStep.STEP_UNAUTHORIZED:
      default:
        return _initialState(context, false);
    }
  }

  Widget _initialState(BuildContext context, bool hasError) {
    _viewModel.usecaseAuth.authStep.value = AuthStep.STEP_UNAUTHORIZED;
    return Column(
      children: [
        Expanded(
            child: SingleChildScrollView(
                controller: scrollController, child: Center(child: _loginFields(hasError)))),
        SizedBox(
          height: 64,
          width: double.infinity,
          child: _continueButtonInitialState(context),
        ),
      ],
    );
  }

  Widget _continueButtonInitialState(BuildContext context) => ElevatedButton(
        style: ElevatedButton.styleFrom(
          onPrimary: colorScheme.secondary,
          onSurface: colorScheme.secondary,
        ),
        onPressed: () async {
          await _viewModel.sendRequestAuth(
            _userBox.value.field.controller!.text,
            _passwordBox.field.controller!.text,
            _datePicker.selectedDate,
          );
          if (_viewModel.usecaseAuth.authStep.value == AuthStep.STEP_AUTHORIZED) {
            await _authCompleted(context);
          }
        },
        child: _loading(),
      );

  Widget _loading() => Observer(
        () => _viewModel.usecaseAuth.isPolledTask.value &&
                    _viewModel.usecasePolledAuth!.isPolling.value ||
                _viewModel.sendButtonClicked.value
            ? _circularIndicator()
            : Text(
                'Continue',
                style: TextStyle(
                  color: colorScheme.onPrimary,
                ),
              ),
      );

  Widget _circularIndicator() => CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(colorScheme.onPrimary),
      );

  Widget _authStepSecurity(bool hasError) {
    final SecurityStep security = _viewModel.usecaseAuth.security.value;

    List<ItemFormField> formItems = security.securityDetails
        .asMap()
        .entries
        .map(
          (entry) => ItemFormField(
            label: entry.value.question,
            icon: Icons.lock_outline,
            initialValue: entry.value.answer,
          ),
        )
        .toList();

    return Center(
      child: Scaffold(
        backgroundColor: colorScheme.background,
        bottomNavigationBar: _continueButtonSecurityQuestions(security, formItems),
        body: Center(
          child: SingleChildScrollView(
            controller: scrollController,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                hasError
                    ? _viewModel.createErrorBox(
                        margin: EdgeInsets.symmetric(horizontal: 32),
                        message: 'One or more answers are incorrect. Kindly check & try again.')
                    : SizedBox(),
                ...formItems,
                const SizedBox(height: 16),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _continueButtonSecurityQuestions(SecurityStep security, List<ItemFormField> formItems) =>
      Container(
        height: 64,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            onPrimary: colorScheme.secondary,
            onSurface: colorScheme.secondary,
          ),
          onPressed: () => _viewModel.requestAuthAddSteps(
              security,
              AuthStepParams(accountUserName: security.nextAuthStepParams.accountUserName),
              LoginSecurityDetails(
                securityDetails: List.generate(
                  security.securityDetails.length,
                  (index) => SecurityDetails(
                    questionId: security.securityDetails[index].questionId,
                    question: security.securityDetails[index].question,
                    answer: formItems[index].field.controller?.text,
                  ),
                ),
              )),
          child: Text(
            'Continue',
            style: TextStyle(
              color: colorScheme.onPrimary,
            ),
          ),
        ),
      );

  Future _authCompleted(BuildContext context) async => await showDialog<void>(
        context: context,
        builder: (context) => SWDialog(() => _viewModel.onAddAnotherLoan(context)(),
            () => _viewModel.onContinueAuthSuccess(context)()),
      );

  Widget _loginFields(bool hasError) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _servicerLogo(),
          const Padding(
            padding: EdgeInsets.only(left: 50),
            child: Text(
              'Enter your loan information',
              style: TextStyle(fontSize: 20),
              textAlign: TextAlign.center,
            ),
          ),
          _viewModel.checkBoxes(hasError),
          SizedBox(height: 32),
          _changeUserBoxLabel(),
          _passwordBox,
          ..._authParamsFields(),
          SizedBox(height: 6),
          _forgotPasswordLink(),
          const SizedBox(height: 64)
        ],
      );

  _changeUserBoxLabel() => _viewModel.isUserIDInsteadOfUserName.value
      ? _userBox.value = ItemFormField(showHint: true, hint: 'User ID')
      : _userBox.value;

  Widget _servicerLogo() => Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(50),
        child: ImageCompat(
          _assets.containsKey(_viewModel.servicer.id)
              ? _assets[_viewModel.servicer.id]!
              : _viewModel.servicer.logoUrl.toString(),
          height: 240,
          fit: BoxFit.fitWidth,
          package: _assets.containsKey(_viewModel.servicer.id) ? "sw_loan_connect" : '',
        ),
      );

  _authParamsFields() {
    _viewModel.authParams.clear();
    _inGreyContainer.clear();

    _viewModel.createTxtControllers(_viewModel.servicer);

    List fields = _viewModel.mapAuthStepsFields((param) {
      _addOrderedElements(param);
      return _checkAddGrayContainer(param);
    }).toList();

    if (_viewModel.servicer.authSteps[0].authParams.length >= 4 &&
        _viewModel.servicer.id! != sallieMaeId) fields.removeLast();

    return fields;
  }

  _checkAddDatePicker(MapEntry<int, AuthParams> param) =>
      _viewModel.servicer.authSteps[0].authParams.length > 4 && param.key == 2
          ? _datePicker
          : _viewModel.servicer.authSteps[0].authParams.length >= 4 && param.key == 2
              ? _datePicker
              : RawItemFormField(
                  hint: param.value.label,
                  showHint: true,
                  controller: param.key <= _viewModel.servicer.authSteps[0].authParams.length
                      ? _viewModel.authParams[param.key]
                      : null);

  _checkAddGrayContainer(MapEntry<int, AuthParams> param) =>
      _viewModel.servicer.authSteps[0].authParams.length > 4 && param.key == 3
          ? Container(
              margin: EdgeInsets.only(top: 16, left: 32, right: 32),
              padding: EdgeInsets.all(32),
              decoration: BoxDecoration(
                  color: Colors.grey, borderRadius: BorderRadius.all(Radius.circular(32))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: _inGreyContainer,
              ),
            )
          : _checkAddDatePicker(param);

  _addOrderedElements(MapEntry<int, AuthParams> param) {
    if (_viewModel.servicer.authSteps[0].authParams.length > 4) {
      switch (param.key) {
        case 3:
          _inGreyContainer.add(RawItemFormField(
              enableMargins: false,
              hint: param.value.label,
              showHint: true,
              controller: _checkParamKey(param) ? _viewModel.authParams[param.key] : null));
          _inGreyContainer.add(Container(
              margin: EdgeInsets.only(top: 16),
              child: Text(
                "OR",
                style: theme.textTheme.headline4,
                textAlign: TextAlign.center,
              )));
          break;
        case 4:
          _inGreyContainer.add(RawItemFormField(
              enableMargins: false,
              hint: param.value.label,
              showHint: true,
              controller: _checkParamKey(param) ? _viewModel.authParams[param.key] : null));
          break;
      }
    }
  }

  _checkParamKey(MapEntry<int, AuthParams> param) =>
      param.key <= _viewModel.servicer.authSteps[0].authParams.length;

  Widget _forgotPasswordLink() => Observer(
        () => _viewModel.canLaunchForgotPassword.value
            ? Padding(
                padding: const EdgeInsets.only(left: 32, top: 10, right: 50),
                child: GestureDetector(
                    onTap: _viewModel.forgotPasswordLink,
                    child: Text('Forgot password?', style: TextStyle(color: colorScheme.primary))),
              )
            : Center(),
      );

  Widget _authError(BuildContext context) {
    _viewModel.setVisibilityError(true);
    _viewModel.scrollToTop(scrollController);
    _viewModel.loginAttempts.value++;
    if (_viewModel.loginAttempts.value >= 3) {
      _viewModel.setVisibilityWarning(true);
    }
    return _viewModel.usecaseAuth.security.value.securityDetailsRequired != null &&
            _viewModel.usecaseAuth.security.value.securityDetailsRequired!
        ? _authStepSecurity(true)
        : _initialState(context, true);
  }
}
