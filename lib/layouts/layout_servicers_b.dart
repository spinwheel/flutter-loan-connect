import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/item_form_field.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/routes/route_unintegrated_login.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_servicers.dart';

class LayoutServicersB extends StatelessWidget {
  const LayoutServicersB(this._viewModel, {Key? key}) : super(key: key);

  final ViewModelServicers _viewModel;

  @override
  Widget build(BuildContext context) {
    _viewModel.getAllServicers();
    return MaterialThemeWrapper(
        child: Scaffold(
      backgroundColor: colorScheme.background,
      appBar: SlimAppBar(
        'Select your loan servicer',
        style: theme.textTheme.bodyText1,
        centerTitle: true,
        onBackPressed: () => back(context),
      ),
      body: _body(context),
    ));
  }

  Widget _body(BuildContext context) => Column(
        children: [
          Row(
            children: [
              Expanded(
                child: _searchbar(),
              ),
            ],
          ),
          Flexible(
            child: Observer(
              () => _viewModel.loadServicerList(context).length <= 0
                  ? _loanNotFound(context)
                  : Padding(
                      padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                      child: GridView.count(
                        crossAxisCount: 2,
                        shrinkWrap: true,
                        mainAxisSpacing: 16,
                        crossAxisSpacing: 16,
                        childAspectRatio: 2.2 / 1,
                        children: _viewModel.loadServicerList(context),
                      ),
                    ),
            ),
          )
        ],
      );

  Widget _searchbar() => Container(
        margin: EdgeInsets.only(bottom: 32),
        child: BlueItemFormField(
            label: 'Search', onChanged: (string) => _viewModel.search.value = string.toLowerCase()),
      );

  _loanNotFound(BuildContext context) => Center(
        child: InkWell(
          onTap: () => routeUnintegrated(context),
          splashFactory: NoSplash.splashFactory,
          highlightColor: Colors.transparent,
          child: Text(
            "Don't see your loan\nservicer?",
            style: TextStyle(
              color: colorScheme.primary,
              fontStyle: theme.textTheme.bodyText1!.fontStyle,
              fontSize: theme.textTheme.bodyText1!.fontSize,
              fontWeight: theme.textTheme.bodyText1!.fontWeight,
              fontFamily: theme.textTheme.bodyText1!.fontFamily,
              height: theme.textTheme.bodyText1!.height,
              locale: theme.textTheme.bodyText1!.locale,
              textBaseline: theme.textTheme.bodyText1!.textBaseline,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      );
}
