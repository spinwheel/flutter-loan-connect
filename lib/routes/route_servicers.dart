import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/layouts/layout_servicers_b.dart';
import 'package:sw_loan_connect/layouts/layout_servicers_default.dart';
import 'package:sw_loan_connect/loan_connect_app.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_landing_page.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_servicers.dart';

routeServicers(BuildContext? context) => navigate(context, RouteServicers());

class RouteServicers extends StatefulWidget {
  RouteServicers({Key? key}) : super(key: key) {
    put<ViewModelServicers>(() => ViewModelServicers());
  }

  @override
  _RouteServicersState createState() => _RouteServicersState();
}

class _RouteServicersState extends State<RouteServicers> with RouteAware {
  @override
  void didChangeDependencies() {
    routeObserverServicers.subscribe(this, ModalRoute.of(context)!);
    super.didChangeDependencies();
  }

  @override
  void didPush() {
    _viewModel.didPush();
    super.didPush();
  }

  @override
  void dispose() {
    get<ViewModelLandingPage>().didPush();
    _viewModel.clearSearchbar();
    routeObserverServicers.unsubscribe(this);
    super.dispose();
  }

  final ViewModelServicers _viewModel = get<ViewModelServicers>();

  @override
  Widget build(BuildContext context) {
    getSafe<IUseCaseAuth>()?.authStep.value = AuthStep.STEP_UNAUTHORIZED;
    switch (get<ParamsLoanConnect>().layout) {
      case Layout.B:
        return LayoutServicersB(_viewModel);
      case Layout.DEFAULT:
      default:
        return LayoutServicersDefault(_viewModel);
    }
  }
}
