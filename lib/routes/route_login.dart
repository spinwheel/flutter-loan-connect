import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/layouts/layout_login_b.dart';
import 'package:sw_loan_connect/layouts/layout_login_default.dart';
import 'package:sw_loan_connect/loan_connect_app.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_login.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_servicers.dart';

routeLogin(BuildContext? context) => navigate(context, RouteLogin());

class RouteLogin extends StatefulWidget {
  RouteLogin({Key? key}) : super(key: key) {
    replace<ViewModelLogin>(() => ViewModelLogin());
  }

  @override
  _RouteLoginState createState() => _RouteLoginState();
}

class _RouteLoginState extends State<RouteLogin> with RouteAware {
  final ViewModelLogin _viewModel = get<ViewModelLogin>();

  @override
  void didChangeDependencies() {
    routeObserverLogin.subscribe(this, ModalRoute.of(context)!);
    super.didChangeDependencies();
  }

  @override
  void didPush() {
    _viewModel.didPush();
    super.didPush();
  }

  @override
  void dispose() {
    get<ViewModelServicers>().didPush();
    routeObserverLogin.unsubscribe(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    switch (getSafe<ParamsLoanConnect>()?.layout) {
      case Layout.B:
        return LayoutLoginB(_viewModel);
      case Layout.DEFAULT:
      default:
        return LayoutLoginDefault(_viewModel);
    }
  }
}
