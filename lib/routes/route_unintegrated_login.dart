import 'package:flutter/material.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/layouts/layout_unintegrated_login_screen_b.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_unintegrated_login.dart';

routeUnintegrated(BuildContext? context) => navigate(context, RouteUnintegratedLogin());

class RouteUnintegratedLogin extends StatefulWidget {
  RouteUnintegratedLogin({Key? key}) : super(key: key) {
    put<ViewModelUnintegratedLogin>(() => ViewModelUnintegratedLogin());
  }

  @override
  _RouteUnintegratedLoginState createState() => _RouteUnintegratedLoginState();
}

class _RouteUnintegratedLoginState extends State<RouteUnintegratedLogin> {
  @override
  Widget build(BuildContext context) => UnintegratedLoginScreen(get<ViewModelUnintegratedLogin>());
}
