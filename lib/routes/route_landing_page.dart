import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/layouts/layout_landing_page_b.dart';
import 'package:sw_loan_connect/layouts/layout_landing_page_default.dart';
import 'package:sw_loan_connect/loan_connect_app.dart';
import 'package:sw_loan_connect/viewmodel/viewmodel_landing_page.dart';

routeLandingPage(BuildContext? context) => navigate(context, RouteLandingPage());

class RouteLandingPage extends StatefulWidget {
  RouteLandingPage({Key? key}) : super(key: key) {
    put<ViewModelLandingPage>(() => ViewModelLandingPage());
  }

  @override
  _RouteLandingPageState createState() => _RouteLandingPageState();
}

class _RouteLandingPageState extends State<RouteLandingPage> with RouteAware {
  final ParamsLoanConnect params = get<ParamsLoanConnect>();

  final ViewModelLandingPage _viewModel = get<ViewModelLandingPage>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserverLandingPage.subscribe(this, ModalRoute.of(context)!);
  }

  @override
  void didPush() {
    _viewModel.didPush();
    super.didPush();
  }

  @override
  void dispose() {
    routeObserverLandingPage.unsubscribe(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (params.build != null) {
      return params.build!(context);
    } else {
      switch (params.layout) {
        case Layout.B:
          return LayoutLandingPageB(_viewModel);
        case Layout.DEFAULT:
        default:
          return LayoutLandingPageDefault(_viewModel);
      }
    }
  }
}
