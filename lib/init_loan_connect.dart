import 'package:sw_auth/sw_init_auth.dart';
import 'package:sw_component/service/service_theme.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/entity_theme/theme_pair.dart';
import 'package:sw_core/interface/service/i_service_theme.dart';
import 'package:sw_core/interface/usecase/i_usecase_all_servicers.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/log.dart';
import 'package:sw_data/inject_spinwheel_api_wrapper.dart';

import 'input/event_handler_loan_connect.dart';

initLoanConnect({
  ParamsLoanConnect? params,
  RequestAuthToken? auth,
  ThemePair? theme,
  String? token,
  Function(EventHandlerLoanConnect)? events,
  bool enableLog = true,
}) async {
  await _primary(params, enableLog, auth, token).whenComplete(() async {
    theme == null
        ? info('No theme was provided. Applying defaults.')
        : put<IServiceTheme>(() => ServiceTheme.fromJson(theme));

    events?.call(put(() => EventHandlerLoanConnect()));

    if (params?.skipToServicerID != null) {
      final servicer = await get<IUseCaseAllServicers>()
          .getServicer(params!.skipToServicerID!)
          .onError((err, stack) => errorWithStack(err ?? 'null', stack));
      replace<Servicer>(() => servicer);
    }
  });
}

Future _primary(
  ParamsLoanConnect? params,
  bool enableLog,
  RequestAuthToken? auth,
  String? token,
) async {
  await initAuth(requestToken: auth, token: token);
  await initData();

  replace<ParamsLoanConnect>(
    () => params ?? ParamsLoanConnect(partnerName: 'Replace at initLoanConnect call'),
  );

  if (enableLog) {
    SWLogger();
  }
}
