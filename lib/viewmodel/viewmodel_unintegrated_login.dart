import 'package:flutter/material.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_auth/auth_step_params.dart';
import 'package:sw_core/entity_auth/communication_details.dart';
import 'package:sw_core/entity_auth/login_security_details.dart';
import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_auth/user_pass.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/request/request_auth.dart';
import 'package:sw_core/request/request_auth_add_steps.dart';
import 'package:sw_core/tool/inject.dart';

class ViewModelUnintegratedLogin {
  final ParamsLoanConnect params = get<ParamsLoanConnect>();

  final isLoading = false.obs;
  final errorBoxVisibility = false.obs;
  final usecaseAuth = getSafe<IUseCaseAuth>();

  Future sendRequestAuth(
    String email,
    String firstName,
    String lastName,
    String loanServicerName,
    String username,
    String password,
    String pin,
    String dob,
    String accountNumber,
  ) async {
    isLoading.value = true;
    await usecaseAuth?.requestAuth(RequestAuth(
        email: CommunicationDetails(email: email),
        loginDetails: UserPass(
            firstName: firstName,
            lastName: lastName,
            emailID: email,
            loanServicerName: loanServicerName,
            username: username,
            password: password,
            pin: pin,
            dob: dob == formatNumbersUSA(DateTime.now().toString()) ? null : dob,
            accountNumber: accountNumber),
        loanServicerName: loanServicerName,
        time: DateTime.now().millisecondsSinceEpoch,
        liabilityType: 'STUDENT_LOAN'));
    isLoading.value = false;
  }

  Future requestAuthAddSteps(
    SecurityStep security,
    AuthStepParams authStepParams,
    LoginSecurityDetails loginSecurityDetails,
    String loanServicerName,
  ) async {
    isLoading.value = true;
    await usecaseAuth!.requestAuthAddSteps(RequestAuthAddSteps(
        loanServicerName: loanServicerName,
        userId: security.userId,
        loginDetails: loginSecurityDetails,
        authStep: security.nextAuthStep,
        authStepParams: authStepParams,
        time: DateTime.now().millisecondsSinceEpoch,
        liabilityType: 'STUDENT_LOAN'));
    isLoading.value = false;
  }

  void setVisibility(bool value) => errorBoxVisibility.value = value;

  void onBackPressed(BuildContext context) {
    usecaseAuth!.authStep.value = AuthStep.STEP_UNAUTHORIZED;
    usecaseAuth!.security.value = SecurityStep();
    back(context);
  }

  scrollToTop(ScrollController controller) =>
      controller.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);

  void reset() {
    setVisibility(false);
    usecaseAuth!.authStep.value = AuthStep.STEP_UNAUTHORIZED;
  }
}
