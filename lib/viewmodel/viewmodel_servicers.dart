import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/item_servicer.dart';
import 'package:sw_core/analytics/analytics_constants.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/interface/i_analytics.dart';
import 'package:sw_core/interface/service/i_service_token.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_data/usecase/use_case_all_servicers.dart';
import 'package:sw_loan_connect/routes/route_login.dart';

final String _visited =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_SERVICER_LIST} ${AnalyticsAct.ACT_LOAN_SERVICERS_PAGE}";

final String _clicked =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_SERVICER_CARD} ${AnalyticsAct.ACT_LOAN_SERVICERS_CLICKED}";

final String _searched =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsAct.ACT_LOAN_SERVICERS_SEARCHED}";

final _serviceToken = get<IServiceToken>();

final String _clientIdKey = "client_id";
final String? _clientIdValue = _serviceToken.clientId;

final String _servicerIdKey = "servicer_id";

final String _servicerNameKey = "servicer_name";

final String _searchTextKey = "search_text";
final String _searchTextValue = get<ViewModelServicers>().search.value;

class ViewModelServicers {
  final UseCaseAllServicers _usecase = get<UseCaseAllServicers>();
  final IAnalytics _analytics = get<IAnalytics>();

  final search = ''.obs;

  final Map<String, String> _assets = {
    "9422554f-0170-4afe-b8df-12aaecc3fdef": "logo_aes.png",
    "e32be248-d43f-4dd4-9e34-05c7b8ab1888": "logo_firstmark.png",
    "bb3e39e7-9dd6-461e-a4f4-c95a1a1e80ef": "logo_nelnet.png",
    "e35a6454-ca66-4b35-a7e7-afb25d03c424": "logo_suntrust.png",
  };

  Future getAllServicers() => _usecase.getAllServicers();

  List<Widget> loadServicerList(BuildContext context) {
    List<ItemServicer> servicers = _usecase.allServicers
        .map(
          (servicer) => ItemServicer(
            servicer: servicer,
            onTap: (servicer) => _onServicerTap(servicer, context),
          ),
        )
        .where(_integrated)
        .where(_matchServicerName)
        .toList();

    servicers = _changeAsset(servicers);

    return servicers;
  }

  bool _matchServicerName(ItemServicer element) {
    if (search.value.trim().isEmpty) {
      return true;
    }
    return element.servicer.name.toLowerCase().contains(search.value);
  }

  bool _integrated(ItemServicer element) => element.servicer.isIntegrated;

  List<ItemServicer> _changeAsset(List<ItemServicer> servicers) => servicers
      .asMap()
      .map(
        (index, itemServicer) => MapEntry(
            index,
            _assets.containsKey(itemServicer.servicer.id)
                ? itemServicer = ItemServicer(
                    onTap: itemServicer.onTap,
                    localImage: _assets[itemServicer.servicer.id],
                    servicer: itemServicer.servicer,
                  )
                : itemServicer),
      )
      .values
      .toList();

  didPush() => _analytics.track(
        _visited,
        properties: {
          _clientIdKey: _clientIdValue,
        },
      );

  clearSearchbar() => search.value = '';

  _onServicerTap(Servicer servicer, BuildContext context) {
    _analytics.track(_clicked, properties: {
      _clientIdKey: _clientIdValue,
      _servicerIdKey: servicer.id,
      _servicerNameKey: servicer.name,
    });
    _analytics.track(
      _searched,
      properties: {
        _clientIdKey: _clientIdValue,
        _searchTextKey: _searchTextValue,
      },
    );
    replace<Servicer>(() => servicer);
    routeLogin(context);
  }
}
