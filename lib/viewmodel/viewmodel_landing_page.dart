import 'package:flutter/cupertino.dart';
import 'package:sw_core/analytics/analytics_constants.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/i_analytics.dart';
import 'package:sw_core/interface/service/i_service_token.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/input/event_handler_loan_connect.dart';
import 'package:sw_loan_connect/routes/route_login.dart';
import 'package:sw_loan_connect/routes/route_servicers.dart';

final String _visited =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_PRIVACY} ${AnalyticsAct.ACT_LOAN_SERVICERS_PAGE}";

final String _privacyPolicyClicked =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsAct.ACT_LOAN_SERVICERS_CLICKED} ${AnalyticsDesc.DESC_END_USER_PRIVACY_POLICY}";

final String _continueButtonClicked =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN_HOME_PAGE} ${AnalyticsDesc.DESC_CONTINUE} ${AnalyticsAct.ACT_LOAN_SERVICERS_CLICKED}";

final _serviceToken = get<IServiceToken>();

final String _clientIdKey = "client_id";
final String? _clientIdValue = _serviceToken.clientId;

final String _locationKey = "location";
final String _locationValue = "Loan Connect Details";

class ViewModelLandingPage {
  final ParamsLoanConnect params = get<ParamsLoanConnect>();
  final IAnalytics _analytics = get<IAnalytics>();

  didPush() => _analytics.track(
        _visited,
        properties: {
          _clientIdKey: _clientIdValue,
        },
      );

  onContinue(BuildContext context) {
    _analytics.track(
      _continueButtonClicked,
      properties: {
        _clientIdKey: _clientIdValue,
      },
    );
    params.skipToServicerID == null ? routeServicers(context) : routeLogin(context);
  }

  privacyPolicyLink() {
    _analytics.track(
      _privacyPolicyClicked,
      properties: {
        _clientIdKey: _clientIdValue,
        _locationKey: _locationValue,
      },
    );
    getSafe<EventHandlerLoanConnect>()?.onPolicy?.call();
    launchURL('https://legal.spinwheel.io/end-user-privacy-policy/');
  }
}
