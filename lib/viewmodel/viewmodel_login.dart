import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/status_box.dart';
import 'package:sw_core/analytics/analytics_constants.dart';
import 'package:sw_core/entity_auth/auth_step_params.dart';
import 'package:sw_core/entity_auth/login_security_details.dart';
import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_auth/user_pass.dart';
import 'package:sw_core/entity_servicer/auth_params.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/i_analytics.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/interface/service/i_service_token.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/interface/usecase/i_usecase_polled_auth.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/request/request_auth.dart';
import 'package:sw_core/request/request_auth_add_steps.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_loan_connect/input/event_handler_loan_connect.dart';
import 'package:sw_loan_connect/tool/servicer_id.dart';

final _serviceToken = get<IServiceToken>();
final _servicer = get<Servicer>();

final String _visited =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_SERVICER_LOGIN} ${AnalyticsAct.ACT_LOAN_SERVICERS_PAGE}";

final String _forgotPasswordLinkClicked =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_CONNECT_PASSWORD} ${AnalyticsAct.ACT_LOAN_SERVICERS_CLICKED}";

final String _connectLoanButtonClicked =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_CONNECT_LOAN_BUTTON} ${AnalyticsAct.ACT_LOAN_SERVICERS_CLICKED}";

final String _authFailed =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_CONNECT_LOAN} ${AnalyticsAct.ACT_LOAN_SERVICERS_FAILED}";

final String _authSuccessful =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_CONNECT_LOAN} ${AnalyticsAct.ACT_LOAN_SERVICERS_SUCCESS}";

final String _continueButtonClickedAuthSuccess =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_CONTINUE} ${AnalyticsAct.ACT_LOAN_SERVICERS_CLICKED}";

final String _addAnotherLoan =
    "${AnalyticsPage.PAGE_LOAN_SERVICERS_LOGIN} ${AnalyticsDesc.DESC_ADD_ANOTHER_ACCOUNT} ${AnalyticsAct.ACT_LOAN_SERVICERS_CLICKED}";

final String _clientIdKey = "client_id";
final String? _clientIdValue = _serviceToken.clientId;

final String _servicerIdKey = "servicer_id";
final String _servicerIdValue = _servicer.id ?? "undefined";

final String _servicerNameKey = "servicer_name";

final String _stepNameKey = "step_name";

final String _errorMessageKey = "error_message";

final String _screenTypeKey = "screenType";

final String _screenTypeFullScreenValue = "modal/full-screen";
final String _screenTypeModal = "modal";

class ViewModelLogin {
  final IAnalytics _analytics = get<IAnalytics>();

  final canLaunchForgotPassword = true.obs;

  final Servicer servicer = get<Servicer>();
  final isUserIDInsteadOfUserName = false.obs;
  final _errorBoxVisibility = false.obs;
  final _warningBoxVisibility = false.obs;
  final sendButtonClicked = false.obs;
  final loginAttempts = 0.obs;

  final usecasePolledAuth = getSafe<IUseCasePolledAuth>();
  final usecaseAuth = get<IUseCaseAuth>();
  final serviceToken = get<IServiceToken>();

  final List<TextEditingController> authParams = [];

  didPush() => _analytics.track(
        _visited,
        properties: {
          _clientIdKey: _clientIdValue,
          _servicerIdKey: _servicerIdValue,
        },
      );

  onAuthSuccessful() => _analytics.track(
        _authSuccessful,
        properties: {
          _clientIdKey: _clientIdValue,
          _servicerIdKey: _servicerIdValue,
          _servicerNameKey: servicer.name,
        },
      );

  _onAuthError() => _analytics.track(
        _authFailed,
        properties: {
          _clientIdKey: _clientIdValue,
          _servicerIdKey: _servicerIdValue,
          _servicerNameKey: servicer.name,
          _stepNameKey: usecaseAuth.authStep.value.name,
          _errorMessageKey: usecaseAuth.error.value.toString(),
        },
      );

  _onContinueButtonClicked() => _analytics.track(
        _connectLoanButtonClicked,
        properties: {
          _clientIdKey: _clientIdValue,
          _stepNameKey: usecaseAuth.authStep.value.name,
        },
      );

  onContinueButtonAuthSuccessClicked() => _analytics.track(
        _continueButtonClickedAuthSuccess,
        properties: {
          _clientIdKey: _clientIdValue,
          _screenTypeKey: _screenTypeFullScreenValue,
        },
      );

  addAnotherLoanClicked() => _analytics.track(_addAnotherLoan, properties: {
        _clientIdKey: _clientIdValue,
        _screenTypeKey: _screenTypeModal,
      });

  Function onAddAnotherLoan(BuildContext context) {
    final onConnectAnotherLoan = getSafe<EventHandlerLoanConnect>()?.onConnectAnotherLoan;

    if (onConnectAnotherLoan != null) {
      return () {
        addAnotherLoanClicked();
        onConnectAnotherLoan();
      };
    }

    return () {
      addAnotherLoanClicked();
      onBackPressed(context);
    };
  }

  Function onContinueAuthSuccess(BuildContext context) {
    final onContinue = getSafe<EventHandlerLoanConnect>()?.onContinue;

    onBackPressed(context);

    if (onContinue != null) {
      return () {
        onContinueButtonAuthSuccessClicked();
        onContinue();
      };
    }

    return () {
      onContinueButtonAuthSuccessClicked();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text("Replace me with a navigation callback!"),
        ),
      );
    };
  }

  applyRules() {
    switch (servicer.id) {
      case ServicerID.GREAT_LAKES:
        return _applyGreatLakesRules();
      case ServicerID.EARNEST:
        return _applyEarnestRules();
    }
  }

  onBackPressed(BuildContext context) {
    usecaseAuth.authStep.value = AuthStep.STEP_UNAUTHORIZED;
    usecaseAuth.security.value = SecurityStep();
    if (get<ParamsLoanConnect>().layout == Layout.DEFAULT) {
      Navigator.of(context, rootNavigator: true).pop(context);
      Navigator.of(context, rootNavigator: true).pop(context);
    } else {
      Navigator.of(context, rootNavigator: true).pop(context);
    }
  }

  forgotPasswordLink() {
    _analytics.track(
      _forgotPasswordLinkClicked,
      properties: {
        _clientIdKey: _clientIdValue,
        _servicerIdKey: _servicerIdValue,
      },
    );
    launchURL(servicer.forgotPasswordUrl);
  }

  Future sendRequestAuth(
    String username,
    String password,
    String dateOfBirth,
  ) async {
    bool ssn = false;
    bool accountNumber = false;
    bool dob = false;
    bool pin = false;
    servicer.authSteps[0].authParams.forEach((element) {
      if (element.id == "ssn") {
        ssn = true;
      }
      if (element.id == "accountNumber") {
        accountNumber = true;
      }
      if (element.id == "dob") {
        dob = true;
      }
      if (element.id == "pin") {
        pin = true;
      }
    });
    sendButtonClicked.value = true;
    await usecaseAuth.requestAuth(
      RequestAuth(
        loginDetails: UserPass(
          username: username,
          userId: username,
          password: password,
          pin: pin ? authParams[1].text : null,
          dob: dob ? dateOfBirth : null,
          ssn: ssn ? authParams[3].text : null,
          accountNumber: accountNumber
              ? authParams[4].text.isNotEmpty
                  ? authParams[4].text
                  : authParams[3].text
              : null,
        ),
        authStep: 'password',
        loanServicerId: servicer.id,
        extUserID: getSafe<IServiceToken>()?.requestToken?.extUserId,
      ),
    );
    sendButtonClicked.value = false;
    _onContinueButtonClicked();
  }

  Widget createErrorBox({String? message, EdgeInsets? margin}) {
    _onAuthError();
    return Center(
      child: Container(
        margin: margin ?? EdgeInsets.only(left: 16, right: 16, top: 20),
        child: Visibility(
          visible: _errorBoxVisibility.value,
          child: StatusBox(
            () => setVisibilityError(false),
            child: Text(
              servicer.authSteps[0].authParams.length > 2
                  ? "Invalid credentials. Kindly recheck and try again."
                  : message ?? "Username or password incorrect. Kindly recheck & try again.",
              style: TextStyle(
                color: const Color(0xff721D24),
              ),
            ),
            width: double.infinity,
          ),
        ),
      ),
    );
  }

  Widget createWarningBox({String? message, String? link, EdgeInsets? margin}) {
    _onAuthError();
    return Center(
      child: Container(
        margin: margin ?? EdgeInsets.only(left: 16, right: 16, top: 20),
        child: Visibility(
          visible: _warningBoxVisibility.value,
          child: StatusBox(
            () => setVisibilityWarning(false),
            child: RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: message ??
                      "Incorrect login details. Please be aware that multiple failed login attempts may cause for your account to be locked out. If you are having troubles with your login details please visit your servicers website to reset your password. ",
                  style: TextStyle(color: Color(0xff721D24)),
                ),
                TextSpan(
                    text: "${link ?? servicer.forgotPasswordUrl}.",
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () async => await launchURL(link ?? servicer.forgotPasswordUrl),
                    style: TextStyle(color: colorScheme.primary))
              ]),
            ),
            closeButtonColor: Colors.yellow,
            borderColor: Colors.yellow,
            boxColor: Colors.yellow[100],
            width: double.infinity,
          ),
        ),
      ),
    );
  }

  void setVisibilityError(bool value) => _errorBoxVisibility.value = value;
  void setVisibilityWarning(bool value) => _warningBoxVisibility.value = value;

  mapAuthStepsFields(Function(MapEntry<int, AuthParams> param) callback) {
    if (servicer.authSteps.isNotEmpty) {
      return servicer.authSteps[0].authParams
          .asMap()
          .entries
          .where((element) => element.value.group != "1")
          .where((element) => element.value.group != "2")
          .map(callback);
    }
    return <Widget>[];
  }

  void createTxtControllers(Servicer servicer) {
    if (servicer.authSteps.isNotEmpty) {
      int paramsQtd = servicer.authSteps[0].authParams.length;

      for (int i = 0; i <= paramsQtd; i++) {
        authParams.add(TextEditingController());
      }
    }
  }

  scrollToTop(ScrollController scrollController) => scrollController.animateTo(0,
      duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);

  Future requestAuthAddSteps(
    SecurityStep security,
    AuthStepParams authStepParams,
    LoginSecurityDetails loginSecurityDetails,
  ) async {
    await usecaseAuth.requestAuthAddSteps(RequestAuthAddSteps(
      userId: security.userId,
      authStep: security.nextAuthStep,
      authStepParams: authStepParams,
      loanServicerId: servicer.id,
      loginDetails: loginSecurityDetails,
    ));
    sendButtonClicked.value = false;
    _onContinueButtonClicked();
  }

  checkBoxes(bool hasError) {
    if (loginAttempts.value >= 3) {
      return Observer(() => createWarningBox());
    } else {
      return Observer(() =>
          hasError || usecasePolledAuth!.error.value.code != null ? createErrorBox() : SizedBox());
    }
  }

  _applyGreatLakesRules() {
    isUserIDInsteadOfUserName.value = true;
  }

  _applyEarnestRules() =>
      usecaseAuth.authStep.value = usecasePolledAuth?.authStep.value ?? AuthStep.STEP_UNAUTHORIZED;
}
